function addMissingKeys(array) {
  const result = [];
  _.forEach(array, (user) => {
    const obj = user;
    if (!('gender' in user)) {
      obj.gender = null;
    }
    if (!('title' in user)) {
      obj.title = null;
    }
    if (!('full_name' in user)) {
      obj.full_name = null;
    }
    if (!('city' in user)) {
      obj.city = null;
    }
    if (!('state' in user)) {
      obj.state = null;
    }
    if (!('country' in user)) {
      obj.country = null;
    }
    if (!('coordinates' in user)) {
      obj.coordinates = {};
      obj.coordinates.latitude = null;
      obj.coordinates.longitude = null;
    }
    if (!('timezone' in user)) {
      obj.timezone = {};
      obj.timezone.offset = null;
      obj.timezone.description = null;
    }
    if (!('email' in user)) {
      obj.email = null;
    }
    if (!('b_date' in user)) {
      obj.b_date = null;
    }
    if (!('age' in user)) {
      obj.age = 0;
    }
    if (!('phone' in user)) {
      obj.phone = 0;
    }
    if (!('picture_large' in user)) {
      obj.picture_large = null;
    }
    if (!('picture_thumbnail' in user)) {
      obj.picture_thumbnail = null;
    }

    if (!('favorite' in user)) {
      obj.favorite = false;
    }
    if (!('course' in user)) {
      obj.course = null;
    }
    if (!('bg_color' in user)) {
      obj.bg_color = null;
    }
    if (!('note' in user)) {
      obj.note = 'none';
    }
    result.push(obj);
  });
  return result;
}

function createUserArray(array1, idCount) {
  let res = [];
  const array = array1;
  const alreadyHasKey = [
    'gender',
    'title',
    'full_name',
    'city',
    'state',
    'country',
    'postcode',
    'coordinates',
    'timezone',
    'email',
    'b_date',
    'age',
    'phone',
    'picture_large',
    'picture_thumbnail',
  ];
  _.forEach(array, (user) => {
    const obj = {};
    _.forEach(Object.keys(user), (key) => {
      if (Object.prototype.hasOwnProperty.call(user, key)) {
        if (alreadyHasKey.includes(key)) {
          obj[key] = user[key];
        }
        if (key === 'name') {
          obj.title = user[key].title;
          obj.full_name = `${user[key].first} ${user[key].last}`;
        }
        if (key === 'location') {
          obj.city = user[key].city;
          obj.state = user[key].state;
          obj.country = user[key].country;
          obj.postcode = user[key].postcode;
          obj.coordinates = user[key].coordinates;
          obj.timezone = user[key].timezone;
        }

        if (key === 'dob') {
          obj.b_date = user[key].date;
          obj.age = user[key].age;
        }
        if (key === 'picture') {
          obj.picture_large = user[key].large;
          obj.picture_thumbnail = user[key].thumbnail;
        }
        if (key === 'favorite') {
          obj.favorite = user[key];
        }
      }
    });
    res.push(obj);
  });

  res = addMissingKeys(res, idCount);

  let id = idCount;
  const resultFinal = [];
  _.forEach(res, (user) => {
    const userFinal = user;
    userFinal.id = id;
    id += 1;
    userFinal.gender = user.gender.charAt(0).toUpperCase() + user.gender.slice(1);
    resultFinal.push(userFinal);
  });
  return resultFinal;
}

function validation(user) {
  const arrayStrings = ['full_name', 'gender', 'country', 'city'];
  let checkValue = true;
  _.forEach(arrayStrings, (check) => {
    if (!(_.isString(user[check]))) {
      checkValue = false;
      return;
    }
    const str = user[check];
    if (!(str[0] === str[0].toUpperCase())) {
      checkValue = false;
    }
  });
  const myArr = user.full_name.split(' ');
  checkValue = checkValue && myArr[1][0] === myArr[1][0].toUpperCase();

  const rePhone = new RegExp(/[+]?[(]?[0-9]+[)]?[-\s.]?[0-9]+[-\s.]?[0-9]+/);
  const reEmail = new RegExp(/([\w-.]+@([\w-]+.)+[\w-]{2,4})?/);

  return (
    checkValue
    && _.isNumber(user.age)
    && rePhone.test(user.phone)
    && reEmail.test(user.email)
  );
}

function filtration(object, array) {
  const result = _.filter(array, (user) => {
    let res = true;
    _.forEach(Object.keys(object), (key) => {
      if (
        'age_min' in object
        && 'age_max' in object
        && (user.age < object.age_min || user.age > object.age_max)
      ) {
        res = false;
      }
      if (Object.prototype.hasOwnProperty.call(user, key)) {
        if (!(object[key] === user[key])) {
          res = false;
        }
      }
    });
    return res;
  });
  return result;
}

let users = [];
let filtered = users;
const maxRows = 10;
let lastPage = Math.ceil(filtered.length / maxRows);
let addTop;
let addFav;
let displayP;
let addTable;
let addCharts;

const obj = {};
const topTeacher = document.querySelector('.grid-5-teachers');
const favTeacher = document.querySelector('.grid-5-teachers-fav');
const teacherTable = document.querySelector('tbody');
const moreTeachers = document.querySelector('#more-teachers');
const filterGender = document.querySelector('#gender-filter');
const filterCountry = document.querySelector('#country-filter');
const filterFav = document.querySelector('#fav-filter');
const searchField = document.querySelector('#search-field');
const ageSearch = document.querySelector('#search-age');
const nameSearch = document.querySelector('#search-name');
const noteSearch = document.querySelector('#search-note');
const searchTeacher = document.querySelector('#search-field');
const addTeacher = document.querySelector('#add-teacher-form');
const filterAgeMin = document.querySelector('#age-min-filter');
const filterAgeMax = document.querySelector('#age-max-filter');

let currentPage = 1;

fetch('https://randomuser.me/api/?results=50')
  .then((response) => response.json())
  .then((data) => {
    users = createUserArray(data.results, users.length);
    filtered = users;
    lastPage = Math.ceil(filtered.length / maxRows);
    addFav();
    addTable();
    addTable();
    addTop();
    displayP();
    addCharts();
  });

function chageCountry(country) {
  let res = '';
  if (country === 'Australia') {
    res = 'AU';
  }
  if (country === 'Brazil') {
    res = 'BR';
  }
  if (country === 'Canada') {
    res = 'CA';
  }
  if (country === 'Switzerland') {
    res = 'CH';
  }
  if (country === 'Germany') {
    res = 'DE';
  }
  if (country === 'Denmark') {
    res = 'DK';
  }
  if (country === 'Spain') {
    res = 'ES';
  }
  if (country === 'Finland') {
    res = 'FI';
  }
  if (country === 'France') {
    res = 'FR';
  }
  if (country === 'United Kingdom') {
    res = 'GB';
  }
  if (country === 'Ireland') {
    res = 'IE';
  }
  if (country === 'Iran') {
    res = 'IR';
  }
  if (country === 'Norway') {
    res = 'NO';
  }
  if (country === 'Netherlands') {
    res = 'NL';
  }
  if (country === 'New Zealand') {
    res = 'NZ';
  }
  if (country === 'Turkey') {
    res = 'TR';
  }
  if (country === 'United States') {
    res = 'US';
  }
  return res;
}

moreTeachers.onclick = function getMoreTeachers() {
  ('here');

  let query = 'https://randomuser.me/api/?results=10';
  const gender = document.querySelector('#gender-filter');
  const genderVal = gender.options[gender.selectedIndex].text.toLowerCase();
  if (gender !== 'All') {
    query += `&gender=${genderVal}`;
  }

  const country = document.querySelector('#country-filter');
  const countryVal = chageCountry(country.options[country.selectedIndex].text);
  if (country !== 'All') {
    query += `&nat=${countryVal}`;
  }
  document.querySelector('#fav-filter');
  document.querySelector('#filter-age');
  const ageMin = parseInt(filterAgeMin.value, 10);
  const ageMax = parseInt(filterAgeMax.value, 10);
  fetch(query)
    .then((response) => response.json())
    .then((data) => {
      const newUsers = createUserArray(data.results, users.length);
      _.forEach(newUsers, (user) => {
        const newAge = parseInt(Math.floor(Math.random() * (ageMax - ageMin)), 10) + ageMin;
        user.age = newAge;
      });
      users = users.concat(newUsers);
      filtered = filtered.concat(newUsers);

      lastPage = Math.ceil(filtered.length / maxRows);
      addFav();
      addTable();
      addTop();
      displayP();
      addCharts();
    });
};
let mapInit = L.map('map', {
  center: [-28.2587, -130.9249],
  zoom: 5,
});

function teacherPopUp(e) {
  const popUpInfo = document.querySelector('#teacher-info');
  const id = e.target.closest('.image-column').getAttribute('data-id');
  window.location.href = '#teacher-info';
  const user = users[id];
  popUpInfo.querySelector('h2').textContent = user.full_name;
  document.getElementById(
    'pop-address',
  ).textContent = `${user.city},  ${user.country}`;
  document.getElementById(
    'pop-age-gend',
  ).textContent = `${user.age},  ${user.gender[0]}`;
  document.getElementById('teach-pic').setAttribute('src', user.picture_large);
  document.getElementById('pop-email').textContent = `${user.email}`;
  document.getElementById('pop-tel').textContent = `${user.phone}`;
  document.getElementById('pop-note').textContent = `${user.note}`;
  if (user.favorite === true) {
    document.getElementById('check-fav').style.color = '#eed70e';
    document.getElementById('check-fav').style.textShadow = '1px 2px 1px #000';
  } else {
    document.getElementById('check-fav').style.color = 'black';
    document.getElementById('check-fav').style.textShadow = '0 0 0 #000';
  }
  document.getElementById('check-fav').onclick = function popUpFunc() {
    if (user.favorite === true) {
      document.getElementById('check-fav').style.color = 'black';
      document.getElementById('check-fav').style.textShadow = '0 0 0 #000';
      users[id].favorite = false;
      addTop();
      addFav();
    } else {
      document.getElementById('check-fav').style.color = '#eed70e';
      document.getElementById('check-fav').style.textShadow = '1px 2px 1px #000';
      users[id].favorite = true;
      addTop();
      addFav();
    }
  };
  mapInit.remove();
  mapInit = L.map('map', {
    center: [users[id].coordinates.latitude, users[id].coordinates.longitude],
    zoom: 5,
  });
  // OSM (Open Street Map)
  const osm = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
  }).addTo(mapInit);
  const marker = L.marker([
    users[id].coordinates.latitude,
    users[id].coordinates.longitude,
  ]).addTo(mapInit);
  const Basemaps = {
    OSM: osm,
  };
  const Overlaymaps = {
    Marker: marker,
  };
  L.control.layers(Basemaps, Overlaymaps).addTo(mapInit);

  const bDay = dayjs(users[id].b_date, 'YYYY-MM-DD');

  const today = new Date();
  const bday = new Date(today.getFullYear(), bDay.get('month')-1, bDay.get('date'));
  if (today.getTime() > bday.getTime()) {
    bday.setFullYear(bday.getFullYear() + 1);
  }
  const nextBDay = dayjs(bday);
  const diff = nextBDay.diff(dayjs(), 'day')+1;
  document.getElementById('pop-bd').textContent = `Birthday: ${bDay.get('year')}-${bDay.get('month')}-${bDay.get('date')}  (${diff} days till next)`;
}

function getInitials(name) {
  const splitted = name.trim().split(' ');
  return `${splitted[0].charAt(0)}.${
    splitted[1] ? `${splitted[1].charAt(0)}.` : ''
  }`;
}

function addTeachersTop() {
  topTeacher.innerHTML = '';
  _.forEach(filtered, (user) => {
    const newTeacher = document.createElement('div');
    newTeacher.addEventListener('click', teacherPopUp, false);
    newTeacher.className = 'image-column';
    newTeacher.setAttribute('data-id', user.id);
    const name = user.full_name.split(' ', 2);
    let str = '<div class="avatar">';
    if (user.favorite === true) {
      str += '<span class="star">&#9733;</span>';
    } else {
      str += '<span class="star" hidden>&#9733;</span>';
    }
    if (user.picture_large) {
      str += `<img src="${user.picture_large}" alt="Teachers picture" /></div></div>`;
    } else {
      str += `<span class="letters">${getInitials(
        user.full_name,
      )}</span></div></div>`;
    }
    str += `<span class="name">${name[0]}</span><span class="name">${name[1]}</span><span class="country">${user.country}</span></div>`;
    newTeacher.innerHTML = str;
    topTeacher.append(newTeacher);
  });
}
addTop = addTeachersTop;

function setPage(page) {
  currentPage = page;
  addTable();
}

function displayPages() {
  document.getElementById('pagination').innerHTML = '';
  if (lastPage > 1) {
    const navElem = document.getElementById('pagination');
    if (currentPage > 1) {
      const elemPrev = document.createElement('span');
      elemPrev.id = 'prevButton';
      elemPrev.textContent = 'Prev';
      elemPrev.onclick = () => {
        if (currentPage > 1) currentPage -= 1;
        addTable();
        displayPages();
      };
      navElem.appendChild(elemPrev);
    }
    for (let i = 1; i <= lastPage; i += 1) {
      const spElem = document.createElement('span');
      if (i === currentPage) {
        spElem.setAttribute('class', 'current');
      }
      spElem.onclick = () => setPage(i);
      spElem.textContent = i;
      navElem.appendChild(spElem);
    }
    if (currentPage < lastPage) {
      const elemNext = document.createElement('span');
      elemNext.id = 'nextButton';
      elemNext.textContent = 'Next';
      elemNext.onclick = () => {
        if (currentPage <= lastPage) currentPage += 1;
        addTable();
        displayPages();
      };
      navElem.appendChild(elemNext);
    }
  } else document.getElementById('pagination').innerHTML = '';
}

displayP = displayPages;

function addTeachersTable() {
  teacherTable.innerHTML = '';
  for (
    let i = maxRows * (currentPage - 1);
    filtered[i] && i < maxRows * (currentPage - 1) + maxRows;
    i += 1
  ) {
    const user = filtered[i];
    let strTable = teacherTable.innerHTML;
    let bdate = 'unknown';
    if (user.b_date !== null) {
      bdate = user.b_date.substr(0, 10);
    }
    strTable += `<tr><td>${user.full_name}</td><td>${user.age}</td><td>${user.gender}</td><td>${user.country}</td><td>${bdate}</td></tr>`;
    teacherTable.innerHTML = strTable;
  }
  displayPages();
}

addTable = addTeachersTable;

function addTeachersFav() {
  favTeacher.innerHTML = '';
  _.forEach(users, (user) => {
    if (user.favorite === true) {
      const newTeacher = document.createElement('div');
      newTeacher.addEventListener('click', teacherPopUp, false);
      newTeacher.className = 'image-column';
      newTeacher.setAttribute('data-id', user.id);
      const name = user.full_name.split(' ', 2);
      let str = '<div class="avatar">';
      if (user.favorite === true) {
        str += '<span class="star">&#9733;</span>';
      } else {
        str += '<span class="star" hidden>&#9733;</span>';
      }
      if (user.picture_large) {
        str += `<img src="${user.picture_large}" alt="Teachers picture" /></div></div>`;
      } else {
        str += `<span class="letters">${getInitials(
          user.full_name,
        )}</span></div></div>`;
      }
      str += `<span class="name">${name[0]}</span><span class="name">${name[1]}</span><span class="country">${user.country}</span></div>`;
      newTeacher.innerHTML = str;
      favTeacher.append(newTeacher);
    }
  });
}
addFav = addTeachersFav;

filterGender.onchange = function filtGenderFunc() {
  const gender = this.options[this.selectedIndex].text;
  if (gender !== 'All') {
    obj.gender = gender;
  } else {
    delete obj.gender;
  }
  filtered = filtration(obj, users);
  addTeachersTop();
  addTeachersTable();
  lastPage = Math.ceil(filtered.length / maxRows);
  displayPages();
};

filterCountry.onchange = function filtCountryFunc() {
  const country = this.options[this.selectedIndex].text;
  if (country !== 'All') {
    obj.country = country;
  } else {
    delete obj.country;
  }
  filtered = filtration(obj, users);
  addTeachersTop();
  addTeachersTable();
  lastPage = Math.ceil(filtered.length / maxRows);
  displayPages();
};

filterFav.onclick = function filtFavFunc() {
  if (this.checked) {
    obj.favorite = this.checked;
  } else {
    delete obj.favorite;
  }
  filtered = filtration(obj, users);
  addTeachersTop();
  addTeachersTable();
  lastPage = Math.ceil(filtered.length / maxRows);
  displayPages();
};

ageSearch.onclick = function searchAgeFunc() {
  searchField.innerHTML = '';
  const str = '<input type="number" id="search" name="search" step="1" value="18" min="18" max="100" /><button class="btn-search">Search</button>';
  searchField.innerHTML = str;
};

nameSearch.onclick = function searchNameFunc() {
  searchField.innerHTML = '';
  const str = '<input type="search" id="search" name="search" placeholder="Enter name to search" aria-label="Search through site content" /><button class="btn-search">Search</button>';
  searchField.innerHTML = str;
};

noteSearch.onclick = function searchNoteFunc() {
  searchField.innerHTML = '';
  const str = '<input type="search" id="search" name="search" placeholder="Enter note to search" aria-label="Search through site content" /><button class="btn-search">Search</button>';
  searchField.innerHTML = str;
};

searchTeacher.onsubmit = function searchTeacherFunc() {
  const { value } = document.querySelector('#search');
  if (document.querySelector('#search-age').checked) {
    const age = parseInt(value, 10);
    filtered = _.filter(users, { age });
  } else if (document.querySelector('#search-note').checked) {
    filtered = _.filter(users, (o) => o.note.includes(value));
  } else {
    filtered = _.filter(users, (o) => o.full_name.toLowerCase().includes(value.toLowerCase()));
  }
  addTeachersTable();
  addTeachersTop();
  currentPage = 1;

  lastPage = Math.ceil(filtered.length / maxRows);
  displayPages();
  return false;
};

addTeacher.onsubmit = async (e) => {
  e.preventDefault();
  const objct = {};
  objct.full_name = `${document.querySelector('#add-name').value} ${
    document.querySelector('#add-last-name').value
  }`;
  objct.age = parseInt(document.querySelector('#add-age').value, 10);
  const countrySelect = document.querySelector('#add-country');
  objct.country = countrySelect.options[countrySelect.selectedIndex].text;
  objct.city = document.querySelector('#add-city').value;
  objct.email = document.querySelector('#add-email').value;
  objct.phone = document.querySelector('#add-phone').value;
  objct.bg_color = document.querySelector('#add-color').value;
  const gender = document.getElementsByName('gender-add');
  for (let i = 0; i < gender.length; i += 1) {
    if (gender[i].type === 'radio' && gender[i].checked) {
      objct.gender = gender[i].value;
    }
  }
  const arr = [objct];
  const objComplete = addMissingKeys(arr)[0];
  if (validation(objComplete)) {
    alert('Teacher added!');
    objct.id = Math.floor(Date.now() / 1000);
    await fetch('http://localhost:3001/posts', {
      method: 'POST',
      body: JSON.stringify(objct),
      headers: { 'Content-Type': 'application/json' },
    });
  } else {
    alert('Wrong values entered!');
  }
  return false;
};

function sortName() {
  const orderCheck = document.querySelector('#sort-dir');
  if (orderCheck.checked) {
    orderCheck.checked = false;
  } else {
    orderCheck.checked = true;
  }
  if (orderCheck.checked) {
    filtered = _.sortBy(users, ['full_name']);
  } else {
    filtered = _.reverse(_.sortBy(users, ['full_name']));
  }
  addTeachersTable();
}

function sortAge() {
  const orderCheck = document.querySelector('#sort-dir');
  if (orderCheck.checked) {
    orderCheck.checked = false;
  } else {
    orderCheck.checked = true;
  }
  if (orderCheck.checked) {
    filtered = _.sortBy(users, ['age']);
  } else {
    filtered = _.reverse(_.sortBy(users, ['age']));
  }
  addTeachersTable();
}

function sortGender() {
  const orderCheck = document.querySelector('#sort-dir');
  if (orderCheck.checked) {
    orderCheck.checked = false;
  } else {
    orderCheck.checked = true;
  }
  if (orderCheck.checked) {
    filtered = _.sortBy(users, ['gender']);
  } else {
    filtered = _.reverse(_.sortBy(users, ['gender']));
  }
  addTeachersTable();
}

function sortCountry() {
  const orderCheck = document.querySelector('#sort-dir');
  if (orderCheck.checked) {
    orderCheck.checked = false;
  } else {
    orderCheck.checked = true;
  }
  if (orderCheck.checked) {
    filtered = _.sortBy(users, ['country']);
  } else {
    filtered = _.reverse(_.sortBy(users, ['country']));
  }
  addTeachersTable();
}

function sortBDay() {
  const orderCheck = document.querySelector('#sort-dir');
  if (orderCheck.checked) {
    orderCheck.checked = false;
  } else {
    orderCheck.checked = true;
  }
  if (orderCheck.checked) {
    filtered = _.sortBy(users, ['b_date']);
  } else {
    filtered = _.reverse(_.sortBy(users, ['b_date']));
  }
  addTeachersTable();
}

function ageChange() {
  const ageMin = filterAgeMin.value;
  const ageMax = filterAgeMax.value;
  obj.age_min = ageMin;
  obj.age_max = ageMax;
  filtered = filtration(obj, users);
  addTeachersTop();
  addTeachersTable();
  lastPage = Math.ceil(filtered.length / maxRows);
  displayPages();
}
filterAgeMin.addEventListener('change', ageChange);
filterAgeMax.addEventListener('change', ageChange);
document.getElementById('table-name').onclick = sortName;
document.getElementById('table-age').onclick = sortAge;
document.getElementById('table-gender').onclick = sortGender;
document.getElementById('table-country').onclick = sortCountry;
document.getElementById('table-bday').onclick = sortBDay;

const countryList = [
  'Australia',
  'Brazil',
  'Canada',
  'Switzerland',
  'Germany',
  'Denmark',
  'Spain',
  'Finland',
  'France',
  'United Kingdom',
  'Ireland',
  'Iran',
  'Norway',
  'Netherlands',
  'New Zealand',
  'Turkey',
  'United States',
];

function addCountry(all) {
  if (all) {
    countryList.splice(0, 0, 'All');
  }
  let str = '';
  countryList.forEach((country) => {
    str += `<option value="${country}">${country}</option>`;
  });
  return str;
}

const countryFilter = document.querySelectorAll('.country');
countryFilter[0].innerHTML = addCountry(true);
countryFilter[1].innerHTML = addCountry(false);
addTeachersTop();
addTeachersFav();
addTeachersTable();

let countryChart;
let genderChart;
let ageChart;

function getGraduses(quant) {
  return (180 * quant) / (initialUsers.length);
}

function addChartsFunc() {

  const countryCanvas = document.getElementById('country-chart').getContext('2d');
  const genderCanvas = document.getElementById('gender-chart').getContext('2d');
  const ageCanvas = document.getElementById('age-chart').getContext('2d');

  countryChart = new Chart(countryCanvas, {
    type: 'pie',
    data: {
      labels: [
        'Australia',
        'Brazil',
        'Canada',
        'Switzerland',
        'Germany',
        'Denmark',
        'Spain',
        'Finland',
        'France',
        'United Kingdom',
        'Ireland',
        'Iran',
        'Norway',
        'Netherlands',
        'New Zealand',
        'Turkey',
        'United States',
      ],
      datasets: [
        {
          backgroundColor: [
            '#ffebe6', '#ffd6cc', '#ffc2b3', '#ffad99', '#ff9980', '#ff8566', '#ff704d', '#ff5c33', '#ff471a', '#ff3300', '#e62e00',
            '#cc2900', '#b32400', '#991f00', '#801a00', '#661400', '#4d0f00',
          ],
          data: [
            _.filter(users, (o) => o.country === 'Australia').length,
            _.filter(users, (o) => o.country === 'Brazil').length,
            _.filter(users, (o) => o.country === 'Canada').length,
            _.filter(users, (o) => o.country === 'Switzerland').length,
            _.filter(users, (o) => o.country === 'Germany').length,
            _.filter(users, (o) => o.country === 'Denmark').length,
            _.filter(users, (o) => o.country === 'Spain').length,
            _.filter(users, (o) => o.country === 'Finland').length,
            _.filter(users, (o) => o.country === 'France').length,
            _.filter(users, (o) => o.country === 'United Kingdom').length,
            _.filter(users, (o) => o.country === 'Ireland').length,
            _.filter(users, (o) => o.country === 'Norway').length,
            _.filter(users, (o) => o.country === 'Netherlands').length,
            _.filter(users, (o) => o.country === 'New Zealand').length,
            _.filter(users, (o) => o.country === 'Turkey').length,
            _.filter(users, (o) => o.country === 'United States').length,
          ],
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: 'By country',
        fontColor: 'black',
        fontFamily: 'Trebuchet MS',
        fontWeight: 300,
        fontSize: 30,
      },
    },
  });

  genderChart = new Chart(genderCanvas, {
    type: 'pie',
    data: {
      labels: ['Female', 'Male',],
      datasets: [
        {
          backgroundColor: [
            '#ffc2b3', '#ff471a',
          ],
          data: [
            _.filter(users, (o) => o.gender === 'Female').length, _.filter(users, (o) => o.gender === 'Male').length,
          ],
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: 'By gender',
        fontColor: 'black',
        fontFamily: 'Trebuchet MS',
        fontWeight: 300,
        fontSize: 30,
      },
    },
  });

  ageChart = new Chart(ageCanvas, {
    type: 'pie',
    data: {
      labels: ['<21', '21-30', '31-40', '41-50', '51-60', '61-70', '>70',],
      datasets: [
        {
          backgroundColor: [
            '#ffebe6', '#ffc2b3', '#ff9980', '#ff704d', '#ff471a', '#e62e00',
            '#b32400', '#801a00', '#661400', '#4d0f00',
          ],
          data: [
            _.filter(users, (o) => o.age <= 20).length,
            _.filter(users, (o) => o.age > 20 && o.age <= 30).length,
            _.filter(users, (o) => o.age > 30 && o.age <= 40).length,
            _.filter(users, (o) => o.age > 40 && o.age <= 50).length,
            _.filter(users, (o) => o.age > 50 && o.age <= 60).length,
            _.filter(users, (o) => o.age > 60 && o.age <= 70).length,
            _.filter(users, (o) => o.age > 70).length,
          ],
        },
      ],
    },
    options: {
      title: {
        display: true,
        text: 'By age',
        fontColor: 'black',
        fontFamily: 'Trebuchet MS',
        fontWeight: 300,
        fontSize: 30,
      },
    },
  });
}

addCharts = addChartsFunc;